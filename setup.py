# -*- coding: utf-8 -*-

from setuptools import setup, find_packages
from shuffler.core import version

data_files = [('share/pixmaps', ['data/shuffler.png']),
              ('share/applications', ['data/shuffler.desktop']),
              ('share/dbus-1/services', ['data/org.mpris.shuffler.service']),
              ]

readme = open('README').read()

setup(name="shuffler",
      version=version,
      description="A little audio player for the masses",
      long_description=readme,
      author="Philippe Normand",
      author_email='phil@base-art.net',
      license="GPL2",
      packages=find_packages(),
      include_package_data=True,
      data_files=data_files,
      url="http://shuffler.base-art.net",
      download_url='http://shuffler.base-art.net/download/shuffler-%s.tar.gz' % version,
      keywords=['multimedia', 'gstreamer', 'telepathy', 'd-bus', 'gtk+',
                'sqlite', 'last.fm'],
      classifiers=['Development Status :: 5 - Production/Stable',
                   'Environment :: Console',
                   'Operating System :: OS Independent',
                   'Programming Language :: Python',
                   ],
      entry_points="""\
      [console_scripts]
      shuffler = shuffler.core.main:run

      [gui_scripts]
      gshuffler = shuffler.clients.gshuffler.main:run
      eshuffler = shuffler.clients.eshuffler.main:run
      """,
)
