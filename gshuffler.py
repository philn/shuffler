
import sys
from shuffler.clients.gshuffler.main import run

if __name__ == '__main__':
    sys.exit(run())
