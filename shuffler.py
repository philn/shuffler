#!/usr/bin/env python

import sys
from shuffler.core.main import run

if __name__ == '__main__':
    sys.exit(run(sys.argv[1:]))
