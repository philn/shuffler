
import sys
from shuffler.clients.eshuffler.main import run

if __name__ == '__main__':
    sys.exit(run())
