
import evas
import edje
import ecore
import ecore.evas
import e_dbus

import sys
import os
from optparse import OptionParser
import pkg_resources

from shuffler.core import log
import dbus
from shuffler.core.constants import BUS_NAME, DB_PATH, DB_INTERFACE, \
     PLAYER_PATH, PLAYER_INTERFACE, TRACK_LIST_PATH, ROOT_PATH
from shuffler.clients.eshuffler.kinetic_list import KineticList

WIDTH = 800
HEIGHT = 480
TITLE = "EShuffler"
WM_NAME = "EShuffler"
WM_CLASS = "main"

def get_dbus_iface(bus, interface, path):
    remote_object = bus.get_object(BUS_NAME, path)
    iface = dbus.Interface(remote_object, interface)
    return iface

def get_root(bus):
    return get_dbus_iface(bus, PLAYER_INTERFACE, ROOT_PATH)

def get_db(bus):
    return get_dbus_iface(bus, DB_INTERFACE, DB_PATH)

def get_player(bus):
    return get_dbus_iface(bus, PLAYER_INTERFACE, PLAYER_PATH)

def get_playlist(bus):
    return get_dbus_iface(bus, PLAYER_INTERFACE, TRACK_LIST_PATH)

class EvasCanvas(object):
    def __init__(self, fullscreen, engine, size):
        if engine == "x11":
            f = ecore.evas.SoftwareX11
        elif engine == "x11-16":
            if ecore.evas.engine_type_supported_get("software_x11_16"):
                f = ecore.evas.SoftwareX11_16
            else:
                print "warning: x11-16 is not supported, fallback to x11"
                f = ecore.evas.SoftwareX11
        elif engine == "gl-x11":
            f = ecore.evas.GLX11

        self.evas_obj = f(w=size[0], h=size[1])
        self.evas_obj.callback_delete_request = self.on_delete_request
        self.evas_obj.callback_resize = self.on_resize

        self.evas_obj.title = TITLE
        self.evas_obj.name_class = (WM_NAME, WM_CLASS)
        self.evas_obj.fullscreen = fullscreen
        self.evas_obj.size = size
        self.evas_obj.show()

    def on_resize(self, evas_obj):
        x, y, w, h = evas_obj.evas.viewport
        size = (w, h)
        for key in evas_obj.data.keys():
            evas_obj.data[key].size = size

    def on_delete_request(self, evas_obj):
        ecore.main_loop_quit()

class TestView(object):

    def __init__(self, evas_canvas, items):
        self.evas_obj = evas_canvas.evas_obj
        #f = os.path.splitext(sys.argv[0])[0] + ".edj"
        data_pkg = "shuffler.clients.eshuffler.data"
        f = pkg_resources.resource_filename(data_pkg, "eshuffler.edj")

        try:
            self.main_group = edje.Edje(self.evas_obj.evas, file=f,
                                        group="main")
        except edje.EdjeLoadError, e:
            raise SystemExit("error loading %s: %s" % (f, e))

        self.main_group.size = self.evas_obj.evas.size
        self.evas_obj.data["main"] = self.main_group

        self.main_group.show()

        self.main_group.on_key_down_add(self.on_key_down)

        self.main_group.focus = True

        self.list_obj = KineticList(self.evas_obj.evas, file=f, item_height=85)
        self.list_obj.freeze()
        for i in items:
            self.list_obj.row_add(i[0], i[1])
        self.list_obj.thaw()

        self.main_group.part_swallow("list", self.list_obj);

    def on_key_down(self, obj, event):
        if event.keyname in ("F6", "f"):
            self.evas_obj.fullscreen = not self.evas_obj.fullscreen
        elif event.keyname == "Escape":
            ecore.main_loop_quit()
        elif event.keyname == "a":
            self.list_obj.scroll(KineticList.SCROLL_PIXELS_UP, 1)
        elif event.keyname == "z":
            self.list_obj.scroll(KineticList.SCROLL_PIXELS_DOWN, 1)
        elif event.keyname == "Up":
            self.list_obj.scroll(KineticList.SCROLL_STEP_BACKWARD)
        elif event.keyname == "Down":
            self.list_obj.scroll(KineticList.SCROLL_STEP_FORWARD)

class EShuffler(log.Loggable):
    log_category = "eshuffler"

    def __init__(self, canvas):
        super(EShuffler, self).__init__()
        self._canvas = canvas

        

    def start(self):
        ecore.main_loop_begin()

    def set_db(self, db):
        self._db = db
        self._init_artist_list()
        #self._init_album_list()

    def get_db(self):
        return self._db

    db = property(fset=set_db, fget=get_db)

    def _init_artist_list(self):
        items = []
        data_pkg = "shuffler.clients.eshuffler.data"
        thumb_path = pkg_resources.resource_filename(data_pkg, "thumb_1.jpg")
        for artist in self.db.ArtistsGet():
            artist_name = artist['name']
            #artist_id = artist['id']
            items.append((artist_name, thumb_path))

        self.view = TestView(self._canvas, items)

def run(*args):
    def parse_geometry(option, opt, value, parser):
        try:
            w, h = value.split("x")
            w = int(w)
            h = int(h)
        except Exception, e:
            raise optparse.OptionValueError("Invalid format for %s" % option)
        parser.values.geometry = (w, h)

    usage = "usage: %prog [options]"
    op = OptionParser(usage=usage)
    op.add_option("-e", "--engine", type="choice",
                  choices=("gl-x11","x11", "x11-16"), default="x11-16",
                  help=("which display engine to use (x11, x11-16), "
                        "default=%default"))
    op.add_option("-n", "--no-fullscreen", action="store_true",
                  help="do not launch in fullscreen")
    op.add_option("-g", "--geometry", type="string", metavar="WxH",
                  action="callback", callback=parse_geometry,
                  default=(800, 480),
                  help="use given window geometry")
    op.add_option("-f", "--fps", type="int", default=50,
                  help="frames per second to use, default=%default")

    # Handle options and create output window
    options, args = op.parse_args()
    edje.frametime_set(1.0 / options.fps)
    canvas = EvasCanvas(fullscreen=not options.no_fullscreen,
                        engine=options.engine,
                        size=options.geometry)
    ui = EShuffler(canvas)

    dbus_ml = e_dbus.DBusEcoreMainLoop()
    bus = dbus.SessionBus(mainloop=dbus_ml)
    ui.root = get_root(bus)
    ui.db = get_db(bus)
    ui.player = get_player(bus)
    ui.playlist = get_playlist(bus)


##     items = []
##     d = os.path.dirname(sys.argv[0])
##     for i in xrange(1000):
##         c = (i % 8) + 1
##         items.append(("Item %d" % i, os.path.join(d, "thumb_%d.jpg" % c)))

##     view = TestView(canvas, items)
    ui.start()

if __name__ == '__main__':
    sys.exit(run(sys.argv[1:]))
