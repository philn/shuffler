from shuffler.core import boot
boot.boot()

import sys
import pkg_resources
import cgi

import urlparse
urlparse.uses_netloc.append('shuffler')

import pygtk
pygtk.require("2.0")
import gtk
import gtk.glade

import gobject
import dbus
from shuffler.core.constants import BUS_NAME, DB_PATH, DB_INTERFACE, \
     PLAYER_PATH, PLAYER_INTERFACE, TRACK_LIST_PATH, ROOT_PATH

from shuffler.core import log
from shuffler.core.telepathy.shuffler_tube_consumer import ShufflerTubeConsumer
from shuffler.core.telepathy import connect

# TODO: proper i18n
_ = lambda text: text

__version__ = '0.1'

def get_dbus_iface(bus, interface, path):
    remote_object = bus.get_object(BUS_NAME, path)
    iface = dbus.Interface(remote_object, interface)
    return iface

def get_root(bus):
    return get_dbus_iface(bus, PLAYER_INTERFACE, ROOT_PATH)

def get_db(bus):
    return get_dbus_iface(bus, DB_INTERFACE, DB_PATH)

def get_player(bus):
    return get_dbus_iface(bus, PLAYER_INTERFACE, PLAYER_PATH)

def get_playlist(bus):
    return get_dbus_iface(bus, PLAYER_INTERFACE, TRACK_LIST_PATH)

class GShuffler(log.Loggable):
    log_category = "gshuffler"

    def __init__(self, is_local=True, client=None):
        log.Loggable.__init__(self)
        self.is_local = is_local
        self.client = client
        data_pkg = "shuffler.clients.gshuffler.data"
        self.gladefile = pkg_resources.resource_filename(data_pkg,
                                                         "gshuffler.glade")
        self.wTree = gtk.glade.XML(self.gladefile)

        bus = dbus.SessionBus()
        remote_object = bus.get_object('org.freedesktop.Notifications',
                                       '/org/freedesktop/Notifications')
        self.notifications = dbus.Interface(remote_object,
                                            'org.freedesktop.Notifications')

        callbacks = ["startingpopup_response_cb",
                     "window_destroy_cb",
                     "quitmenu_activate_cb",
                     "artistview_row_activated_cb",
                     "albumview_row_activated_cb",
                     "volume_value_changed_cb",
                     "speed_value_changed_cb",
                     "tempo_value_changed_cb",
                     "position_adjust_bounds_cb",
                     "playpausebutton_clicked_cb",
                     "prevbutton_clicked_cb",
                     "nextbutton_clicked_cb",
                     "stopbutton_clicked_cb",
                     "randombutton_clicked_cb",
                     "loopbutton_clicked_cb",
                     "smartbutton_clicked_cb",
                     "tracklabel_clicked_cb",
                     "aboutmenu_activate_cb",
                     "aboutdialog_response_cb",
                     "playlistview_row_activated_cb",
                     ]
        dic = dict([(k, getattr(self, k)) for k in callbacks])
        self.wTree.signal_autoconnect(dic)

        self._win_position = None

        tray = gtk.status_icon_new_from_icon_name("audio-x-generic")
        tray.connect('activate', self._tray_button_clicked_cb)

        self._player = None
        self._playlist = None
        self._db = None
        self._root = None

        popup = self.wTree.get_widget('startingpopup')
        prim_label = self.wTree.get_widget('primary-label')
        sec_label = self.wTree.get_widget('secondary-label')
        if is_local:
            msg = _("Connecting to session D-Bus")
        else:
            msg = _("Connecting to Telepathy D-Tubes")
        prim_label.set_markup("<b>%s</b>" % _("Connecting to Shuffler. Please wait"))
        sec_label.set_text(msg)

        popup.show_all()
        self._checking_objects = gobject.timeout_add(200, self._check_objects)

    def _check_objects(self):
        self.wTree.get_widget('popupprogressbar').pulse()
        if None in (self.root, self.db, self.player, self.playlist):
            return True

        popup = self.wTree.get_widget('startingpopup')
        popup.hide_all()

        window = self.wTree.get_widget('window')
        window.show_all()
        #self._checking_objects = False
        gobject.source_remove(self._checking_objects)
        self._checking_objects = False
        return False

    def startingpopup_response_cb(self, widget, response_id):
        if response_id in (gtk.RESPONSE_CANCEL, gtk.RESPONSE_DELETE_EVENT):
            if self.client:
                self.client.stop()
            sys.exit(1)

    def aboutmenu_activate_cb(self, widget):
        about = self.wTree.get_widget('aboutdialog')
        about.set_version(__version__)
        about.show_all()

    def aboutdialog_response_cb(self, widget, response_id):
        if response_id in (gtk.RESPONSE_CANCEL, gtk.RESPONSE_DELETE_EVENT):
            about = self.wTree.get_widget('aboutdialog')
            about.hide_all()

    def _tray_button_clicked_cb(self, widget):
        if not self._checking_objects:
            window = self.wTree.get_widget('window')
            if window.get_property('visible'):
                # store position
                self._win_position = window.get_position()
                window.hide()
            else:
                # restore position
                window.move(self._win_position[0], self._win_position[1])
                window.show()

    def _quit_or_hide(self):
        window = self.wTree.get_widget('window')
        gtk.main_quit()

    def window_destroy_cb(self, widget):
        self._quit_or_hide()

    def quitmenu_activate_cb(self, widget):
        self._quit_or_hide()

    def set_root(self, root):

        def got_identity(identity):
            about = self.wTree.get_widget('aboutdialog')
            about.set_comments(_("A GTK+ client for %s" % identity))

        root.Identity(reply_handler=got_identity,
                      error_handler=self._got_dbus_error)

        self._root = root

    def get_root(self):
        return self._root

    root = property(fset=set_root, fget=get_root)

    def set_player(self, player):
        self._player = player
        self._init_player_zone()

    def get_player(self):
        return self._player

    player = property(fset=set_player, fget=get_player)

    def set_playlist(self, playlist):
        self._playlist = playlist
        self._init_playlist_zone()

    def get_playlist(self):
        return self._playlist

    playlist = property(fset=set_playlist, fget=get_playlist)

    def set_db(self, db):
        self._db = db
        self._init_artist_list()
        self._init_album_list()

    def get_db(self):
        return self._db

    db = property(fset=set_db, fget=get_db)

    def _init_artist_list(self):
        artistview = self.wTree.get_widget('artistview')

        self.artist_treestore = gtk.TreeStore(int, str)
        for artist in self.db.ArtistsGet():
            artist_name = artist['name']
            artist_id = artist['id']
            self.artist_treestore.append(None, [artist_id, artist_name,])

        artistview.set_model(self.artist_treestore)

        tvcolumn = gtk.TreeViewColumn('Artists')
        artistview.append_column(tvcolumn)

        cell = gtk.CellRendererText()
        tvcolumn.pack_start(cell, True)

        tvcolumn.add_attribute(cell, 'text', 1)
        artistview.set_search_column(1)

        artistview.set_headers_visible(False)

    def _init_playlist_list(self):
        playlistview = self.wTree.get_widget('playlistview')

        self.playlist_treestore = gtk.TreeStore(int, str)

        self.playlist_treestore.append(None, [0, _('Current playlist')])

        playlistview.set_model(self.playlist_treestore)

        tvcolumn = gtk.TreeViewColumn('Playlists')
        playlistview.append_column(tvcolumn)

        cell = gtk.CellRendererText()
        tvcolumn.pack_start(cell, True)

        tvcolumn.add_attribute(cell, 'text', 1)
        playlistview.set_search_column(1)

        playlistview.set_headers_visible(False)

    def _init_album_list(self):
        albumview = self.wTree.get_widget('albumview')
        self.album_treestore = gtk.TreeStore(str, int, str, str, str)

        albumview.set_model(self.album_treestore)

        tvcolumn = gtk.TreeViewColumn('Track')
        albumview.append_column(tvcolumn)
        cell = gtk.CellRendererText()
        tvcolumn.pack_start(cell, True)
        tvcolumn.add_attribute(cell, 'text', 2)

        tvcolumn = gtk.TreeViewColumn('Name')
        albumview.append_column(tvcolumn)
        cell = gtk.CellRendererText()
        tvcolumn.pack_start(cell, True)
        tvcolumn.add_attribute(cell, 'markup', 3)

    def _update_albumview(self, playlist_id=None,
                          artist_id=None, album_id=None, track_id=None):
        albumview = self.wTree.get_widget('albumview')

        self.album_treestore.clear()

        def got_albums(albums):
            idx = 0
            for album in albums:
                if album_id is not None and album['id'] == album_id:
                    idx = albums.index(album)
                album_name = "<b>%s</b>" % cgi.escape(album['name'])
                length = ""
                parent = self.album_treestore.append(None, ["album", album['id'],
                                                            '',
                                                            album_name,''])
            if album_id:
                albumview = self.wTree.get_widget('albumview')
                selection = albumview.get_selection()
                selection.select_path((idx,))
                albumview.set_data('track_id', track_id)
                albumview.emit('row-activated',(idx,), None)

        def got_tracks(tracks):
            idx = 0
            for track in tracks:
                track_uri = "file://%s" % track['path']
                title = "%s - %s" % (track['album'], track['title'])
                self.album_treestore.append(None,
                                            ["track", track['id'],
                                             idx, title, track_uri])
                idx += 1


        if artist_id is not None:
            self.db.AlbumsGet(artist_id, reply_handler=got_albums,
                              error_handler=self._got_dbus_error)
        elif playlist_id is not None:
            if playlist_id == 0:
                self.playlist.GetTracks(reply_handler=got_tracks,
                                        error_handler=self._got_dbus_error)

    def _update_tracks(self, album_id, parent, path, model, track_id=None):
        def expand():
            albumview = self.wTree.get_widget('albumview')
            albumview.expand_row(path, False)

        def got_tracks(tracks):
            idx = 0
            for track in tracks:
                if track_id is not None and track['id'] == track_id:
                    idx = tracks.index(track)
                track_uri = "file://%s" % track['path']
                self.album_treestore.append(parent,
                                            ["track", track['id'],
                                             track['trackno'],
                                             track['title'], track_uri])
            expand()

            if track_id:
                albumview = self.wTree.get_widget('albumview')
                selection = albumview.get_selection()
                selection.select_path(path + (idx,))


        def got_status(status):
            play_now = status[0] != 0
            self.playlist.EnqueueAlbum(album_id, play_now)
            expand()

        if not model.iter_has_child(parent):
            self.db.TracksGet(album_id, reply_handler=got_tracks,
                              error_handler=self._got_dbus_error)
        else:
            self.player.GetStatus(reply_handler=got_status,
                                  error_handler=self._got_dbus_error)

    def _init_player_zone(self):
        self.player.connect_to_signal("TrackChange", self._track_changed)
        self.player.connect_to_signal("StatusChange", self._status_changed)
        self._update_player(True)
        self._player_timeout = gobject.timeout_add(1000, self._update_player)

    def _init_playlist_zone(self):

        def got_loop(loop):
            self.wTree.get_widget('loopbutton').set_active(loop)

        def got_random(random):
            self.wTree.get_widget('randombutton').set_active(random)

        def got_smart(smart):
            self.wTree.get_widget('smartbutton').set_active(smart)

        self.playlist.GetLoop(reply_handler=got_loop,
                              error_handler=self._got_dbus_error)

        self.playlist.GetRandom(reply_handler=got_random,
                                error_handler=self._got_dbus_error)

        self.playlist.GetSmart(reply_handler=got_smart,
                               error_handler=self._got_dbus_error)

        # fill playlistview treeview
        self._init_playlist_list()

    def _track_changed(self, track):
        self._update_player(True, True)

    def _status_changed(self, status):
        player_state = status[0]
        state = {0: _("playing"),
                 1: _("paused"),
                 2: _("stopped")}[player_state]
        summary = _("Shuffler now %(state)s" % locals())
        body = ""
        self._notify(summary, body)

    def _format_mseconds(self, mseconds):
        seconds = mseconds / 1000.
        formatted = "%02d:%02d" % (seconds / 60, seconds % 60)
        return formatted

    def _got_dbus_error(self, error):
        self.warning("DBus error: %r", error)

    def _notify(self, summary, body, timeout=5000):
        self.notifications.Notify("gshuffler", 2, "", summary, body,
                                  [], {}, timeout)

    def _update_player(self, full=False, notify=False):
        progressbar = self.wTree.get_widget('position')

        def got_metadata(metadata):
            if not metadata:
                label = self.wTree.get_widget('artistalbumlabel')
                label.set_text(_("Nothing playing"))
                return True

            duration = 0

            if full:

                if notify:
                    # send desktop notification
                    summary = _("Now playing")
                    body = _("<b>%(title)s</b> from <b>%(album)s</b> by <b>%(artist)s</b>")
                    self._notify(summary, body % metadata)

                label = self.wTree.get_widget('artistalbumlabel')
                label.set_markup(_("<b>%s</b> <i>by</i> <b>%s</b>" %
                                   (cgi.escape(metadata['album']),
                                   cgi.escape(metadata['artist']))))

                tracklabel = self.wTree.get_widget('tracklabel')
                tracklabel.set_label(metadata['title'])
                uri = "shuffler://browse?artist_id=%s&album_id=%s&track_id=%s" \
                      % (metadata['artist_id'], metadata['album_id'],
                         metadata['id'])
                tracklabel.set_uri(uri)

                ratinglabel = self.wTree.get_widget('ratinglabel')
                ratinglabel.set_text(_("Rating: %s" % float(metadata['rating'])))

                duration = metadata['length']
                progressbar.max_value = duration
                progressbar.set_range(0, duration)

            def got_position(position):
                progressbar.set_value(position)

                duration = progressbar.max_value

                position_str = self._format_mseconds(position)
                duration_str = self._format_mseconds(duration)
                position_label = self.wTree.get_widget('positionlabel2')
                txt = _("Position: %s / %s" % (position_str,
                                               duration_str))

                statusbar = self.wTree.get_widget("statusbar")
                ctx_id = statusbar.get_context_id("status")
                statusbar.push(ctx_id, txt)


            self.player.PositionGet(reply_handler=got_position,
                                    error_handler=self._got_dbus_error)

        def got_volume(volume):
            self.wTree.get_widget('volume').set_value(volume)

        def got_speed(speed):
            self.wTree.get_widget('speed').set_value(speed)

        def got_tempo(tempo):
            self.wTree.get_widget('tempo').set_value(tempo)

        def got_status(status):
            status = status[0]
            playpause_img = self.wTree.get_widget('playpause_img')
            if status == 0:
                playpause_img.set_from_stock(gtk.STOCK_MEDIA_PAUSE,
                                             gtk.ICON_SIZE_SMALL_TOOLBAR)
            else:
                playpause_img.set_from_stock(gtk.STOCK_MEDIA_PLAY,
                                             gtk.ICON_SIZE_SMALL_TOOLBAR)


        self.player.GetMetadata(reply_handler=got_metadata,
                                error_handler=self._got_dbus_error)

        self.player.VolumeGet(reply_handler=got_volume,
                              error_handler=self._got_dbus_error)

        self.player.SpeedGet(reply_handler=got_speed,
                             error_handler=self._got_dbus_error)

        self.player.TempoGet(reply_handler=got_tempo,
                             error_handler=self._got_dbus_error)

        self.player.GetStatus(reply_handler=got_status,
                              error_handler=self._got_dbus_error)
        return True

    def artistview_row_activated_cb(self, treeview, path, view_column):
        model, treeiter = treeview.get_selection().get_selected()
        artist_id = model.get_value(treeiter, 0)
        self._update_albumview(artist_id=artist_id)

    def playlistview_row_activated_cb(self, treeview, path, view_column):
        model, treeiter = treeview.get_selection().get_selected()
        playlist_id = model.get_value(treeiter, 0)
        self._update_albumview(playlist_id=playlist_id)

    def albumview_row_activated_cb(self, treeview, path, view_column,
                                   ):
        model, treeiter = treeview.get_selection().get_selected()
        item_type = model.get_value(treeiter, 0)

        track_id = treeview.get_data('track_id')
        if item_type == "track":
            track_path = model.get_value(treeiter, 4)
            self._playtrack(track_path)
        else:
            item_id = model.get_value(treeiter, 1)
            self._update_tracks(item_id, treeiter, path, treeview.get_model(),
                                track_id=track_id)

    def _playtrack(self, uri):
        play_now = True
        self.playlist.AddTrack(uri, play_now, ignore_reply=True)

    def tracklabel_clicked_cb(self, widget):
        uri = urlparse.urlparse(widget.get_uri())
        path, params = uri.path.split('?')
        key_values = params.split('&')
        params = dict([map(unicode, i.split('=')) for i in key_values])

        artist_id = int(params['artist_id'])
        album_id = int(params['album_id'])
        track_id = int(params['track_id'])

        # select the correct artist in the artist list
        idx = -1
        cur_idx = 0
        for row in self.artist_treestore:
            if row.model[cur_idx][0] == artist_id:
                idx = cur_idx
                break
            cur_idx += 1
        if idx > -1:
            artistview = self.wTree.get_widget('artistview')
            selection = artistview.get_selection()
            selection.select_path((idx,))

        self._update_albumview(artist_id=artist_id, album_id=album_id,
                               track_id=track_id)

    def volume_value_changed_cb(self, widget, value):
        self.player.VolumeSet(widget.get_value(), ignore_reply=True)

    def speed_value_changed_cb(self, widget):
        self.player.SpeedSet(float(widget.get_value()), ignore_reply=True)

    def tempo_value_changed_cb(self, widget):
        self.player.TempoSet(float(widget.get_value()), ignore_reply=True)

    def position_adjust_bounds_cb(self, widget, value):
        self.player.PositionSet(int(widget.get_value()), ignore_reply=True)

    def playpausebutton_clicked_cb(self, widget):
##         status = self.player.GetStatus()[0]
##         if status == 1:
##             self.player.Play()
##         else:
##             self.player.Pause()
        self.player.TogglePlayPause(ignore_reply=True)

    def nextbutton_clicked_cb(self, widget):
        self.player.Next(ignore_reply=True)

    def prevbutton_clicked_cb(self, widget):
        self.player.Prev(ignore_reply=True)

    def stopbutton_clicked_cb(self, widget):
        self.player.Stop(ignore_reply=True)

    def randombutton_clicked_cb(self, widget):
        self.playlist.SetRandom(widget.get_active(), ignore_reply=True)

    def loopbutton_clicked_cb(self, widget):
        self.playlist.SetLoop(widget.get_active(), ignore_reply=True)

    def smartbutton_clicked_cb(self, widget):
        self.playlist.SetSmart(widget.get_active(), ignore_reply=True)

def local_run(*args):
    ui = GShuffler(True)
    bus = dbus.SessionBus()
    ui.root = get_root(bus)
    ui.db = get_db(bus)
    ui.player = get_player(bus)
    ui.playlist = get_playlist(bus)

    gtk.main()

def remote_run(*args):

    def found_peer(peer):
        if peer.service == DB_INTERFACE:
            ui.db = peer.remote_object_proxy
        elif peer.service == PLAYER_INTERFACE:
            ui.root = peer.remote_shuffler_proxy
            ui.player = peer.remote_object_proxy
            ui.playlist = peer.remote_playlist_proxy

    def disapeared_peer(peer):
        pass

    # FIXME: hardcoded stuff
    nickname = "client2"
    conn = connect.tp_connect("salut", "local-xmpp",
                              {"first-name": nickname,
                               "last-name": "",
                               "published-name": nickname, "nickname": nickname})
    cli = ShufflerTubeConsumer(conn, "Shuffler",
                               found_peer_callback=found_peer,
                               disapeared_peer_callback=disapeared_peer)

    ui = GShuffler(False, cli)
    cli.start()

    try:
        gtk.main()
    finally:
        cli.stop()

def run(args=None):
    if not args:
        args = sys.argv[1:]

    if '-r' in args:
        args.remove('-r')
        remote_run(*args)
    else:
        local_run(*args)

    return 0
        
if __name__ == "__main__":
    sys.exit(run())
