from shuffler.core.telepathy import tube
from shuffler.core.constants import PLAYER_INTERFACE, PLAYER_PATH, \
     DB_PATH, DB_INTERFACE, ROOT_PATH, TRACK_LIST_PATH

from shuffler.core import main
from shuffler.core import player, playlist
from shuffler.core import db

class ShufflerTubeConsumer(tube.TubeConsumer):
    
    def _create_peer_remote_object(self, peer, interface):
        if interface == PLAYER_INTERFACE:
            peer.remote_object = player.Player(self.tube_conn, PLAYER_PATH)
            peer.remote_playlist = peer.remote_object.queue
            peer.remote_shuffler = main.Shuffler(self.tube_conn)
        elif interface == DB_INTERFACE:
            peer.remote_object = db.DB(self.tube_conn, None)
            
    def _create_peer_object_proxy(self, peer, interface):
        found_peer = False
        if interface == PLAYER_INTERFACE:
            found_peer = True
            proxy = peer.remote_object.tube.get_object(peer.initiator_contact,
                                                       PLAYER_PATH)
            peer.remote_object_proxy = proxy
            
            # playlist
            remote_playlist = peer.remote_playlist
            playlist_proxy = remote_playlist.tube.get_object(peer.initiator_contact,
                                                             TRACK_LIST_PATH)
            peer.remote_playlist_proxy = playlist_proxy
            
            # shuffler
            remote_shuffler = peer.remote_shuffler
            shuffler_proxy = remote_shuffler.tube.get_object(peer.initiator_contact,
                                                             ROOT_PATH)
            peer.remote_shuffler_proxy = shuffler_proxy
        elif interface == DB_INTERFACE:
            found_peer = True
            proxy = peer.remote_object.tube.get_object(peer.initiator_contact,
                                                       DB_PATH)
            peer.remote_object_proxy = proxy
            
        if found_peer:
            self.found_peer_callback(peer)
