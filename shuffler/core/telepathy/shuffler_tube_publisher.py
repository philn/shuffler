
import telepathy

from shuffler.core.telepathy import tube
from shuffler.core.constants import PLAYER_INTERFACE, PLAYER_PATH, \
     DB_PATH, DB_INTERFACE, TRACK_LIST_PATH, ROOT_PATH
from shuffler.core import player_controller

class ShufflerTubePublisher(tube.TubePublisher):
    def __init__(self, connection, chatroom, tubes_to_offer, shuffler):
        super(ShufflerTubePublisher, self).__init__(connection, chatroom,
                                                    tubes_to_offer)
        self._shuffler = shuffler
        player = self._shuffler.player
        player.connect('now-playing', self._now_playing)
        self._controller = player_controller.PlayerController(player)

    def _now_playing(self, obj, media):
        txt = "Now playing %(title)s from %(artist)s in %(album)s album"
        self.send_text(txt % media)

    def tube_opened(self, id):
        super(ShufflerTubePublisher, self).tube_opened(id)

        self._shuffler.add_to_connection(self.tube_conn, ROOT_PATH)
        self._shuffler.db.add_to_connection(self.tube_conn, DB_PATH)
        self._shuffler.player.add_to_connection(self.tube_conn, PLAYER_PATH)
        self._shuffler.player.queue.add_to_connection(self.tube_conn,
                                                      TRACK_LIST_PATH)

    def received_cb(self, id, timestamp, sender, type, flags, text):
        super(ShufflerTubePublisher, self).received_cb(id, timestamp,
                                                       sender, type, flags, text)
        contact = self.conn[telepathy.CONN_INTERFACE].InspectHandles(
            telepathy.HANDLE_TYPE_CONTACT, [sender])[0]

        result = self._controller.handle(command=text,
                                         origin=contact)
        if result:
            self.send_text(result)
