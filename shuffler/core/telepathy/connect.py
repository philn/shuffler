
import telepathy
from telepathy.interfaces import CONN_MGR_INTERFACE
import dbus

def to_dbus_account(account):
    for key, value in account.iteritems():
        if value.lower() in ("false", "true"):
            value = bool(value)
        else:
            try:
                value = dbus.UInt32(int(value))
            except:
                pass
        account[key] = value
    return account

def get_config(shuffler):
    cfg = shuffler.config.telepathy
    account = dict(cfg.account)
    manager = cfg.manager
    protocol = cfg.protocol
    chatroom = cfg.chatroom
    return manager, protocol, account, chatroom

global CONNECTION
CONNECTION=None

def tp_connect(manager, protocol, account):
    account = to_dbus_account(account)
    reg = telepathy.client.ManagerRegistry()
    reg.LoadManagers()

    mgr = reg.GetManager(manager)
    connection = mgr[CONN_MGR_INTERFACE].RequestConnection(protocol,
                                                           account)
    conn_bus_name, conn_object_path = connection
    client_connection = telepathy.client.Connection(conn_bus_name,
                                                    conn_object_path,
                                                    ready_handler=None)
    return client_connection

def get_connection(shuffler):
    global CONNECTION

    if not CONNECTION:
        manager, protocol, account, chatroom = get_config(shuffler)

        client_connection = tp_connect(manager, protocol, account)
        CONNECTION = (client_connection, chatroom)

    return CONNECTION
