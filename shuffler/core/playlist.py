
from shuffler.core import log
from shuffler.core.constants import BUS_NAME, TRACK_LIST_PATH
from shuffler.core.dbus_api import track_list
from shuffler.core.smart_queue import SmartQueue

import gobject
import dbus.gobject_service

class Playlist(log.Loggable, dbus.gobject_service.ExportedGObject,
               track_list.DBusTrackListAPI,
               ):
    SUPPORTS_MULTIPLE_CONNECTIONS = True
    default_config = {}

    __gsignals__ = {
        'track-added' : (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE,
                         (gobject.TYPE_PYOBJECT, gobject.TYPE_BOOLEAN,)),
        'track-removed': (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE,
                          (gobject.TYPE_INT,)),
        'current-item-changed': (gobject.SIGNAL_RUN_FIRST, gobject.TYPE_NONE,
                                 (gobject.TYPE_PYOBJECT,)),
    }
    
    def __init__(self, bus, db, conf=None, smart_queue_conf=None):
        log.Loggable.__init__(self)
        
        try:
            bus_name = dbus.service.BusName(BUS_NAME, bus)
        except:
            bus_name = None
            self.tube = bus
        else:
            self.tube = None
        dbus.gobject_service.ExportedGObject.__init__(self, bus,
                                                      bus_name=bus_name,
                                                      object_path=TRACK_LIST_PATH)

        
        conf = conf or self.default_config
        self.config = conf
        self.db = db
        self._index = -1
        self._random = False
        self._smart = False
        self._loop = False
        self._smart_queue = SmartQueue(db, smart_queue_conf)
        self._track_list = []
        
        self.current_media_id = None
        self.current_uri = None
        self.current_short_name = None

        if 'smart' in self.config and int(self.config['smart']):
            self.smart = True
        
    def set_random(self, value):
        self._random = value
        if self.smart:
            self.smart = False
        if self.loop:
            self.loop = False
            
    def get_random(self):
        return self._random

    random = gobject.property(getter=get_random, setter=set_random,
                              type=bool, default=False)

    def set_smart(self, value):
        self._smart = value
        if self.loop:
            self.loop = False
        if self.random:
            self.random = False
            
    def get_smart(self):
        return self._smart

    smart = gobject.property(getter=get_smart, setter=set_smart, type=bool,
                             default=False)

    def set_loop(self, value):
        self._loop = value
        if self.smart:
            self.smart = False
        if self.random:
            self.random = False
            
    def get_loop(self):
        return self._loop

    loop = gobject.property(getter=get_loop, setter=set_loop, type=bool,
                            default=False)

    def get_current_item(self):
        """ Return the current uri of the playing queue as a tuple
        (uri, short_name)
        """
        if self.smart:
            return self._smart_queue.get_current_item()
        else:
            return (self.current_media_id, self.current_uri,
                    self.current_short_name)

    def set_current_item(self, media_id, uri, short_name):
        """ Set the current uri of the playing queue as a tuple (uri,
        short_name)
        """
        if self.smart:
            self._smart_queue.set_current_item(media_id, uri, short_name)
        else:
            self.current_media_id = media_id
            self.current_uri = uri
            self.current_short_name = short_name
            try:
                self._index = self._track_list.index((media_id, uri, short_name))
            except:
                self._index = -1
        self.emit('current-item-changed', (media_id, uri, short_name))

    def previous_item(self):
        if self.smart:
            self._smart_queue.previous_item()
        else:
            if self.random:
                self.next_item()
            elif self._index > 0:
                self._index -= 1
            elif self.loop:
                self._index = len(self._track_list)

            item = self._track_list[self._index]
            self.set_current_item(*item)
            
        return self.get_current_item()
    
    def next_item(self, user_request=False):
        if self.smart:
            self._smart_queue.next_item(user_request=user_request)
            item = self.get_current_item()
            self.set_current_item(*item)
        else:
            if self.random:
                self._index = random.randint(0, len(self._track_list))
            elif self._index < len(self._track_list)-1:
                self._index += 1
            elif self.loop:
                self._index = 0
            else:
                self._index = -1

            item = (None, None, None)
            if self._track_list and self._index > -1:
                item = self._track_list[self._index]
                if item != self.get_current_item():
                    self.set_current_item(*item)
            else:
                self.set_current_item(*item)

        return item
    
    def tracks(self):
        if self.smart:
            tracks = []
        else:
            tracks = self._track_list
        return tracks
        
    def add_track(self, uri, play_now):
        media = None
        if uri.startswith('file://'):
            path = uri[7:]
            media = self.db.get_media_with_path(path)
        if media:
            self._track_list.append((media.id, uri, media.title))
            self.emit('track-added', media, play_now)
            
    def del_track(self, index):
        try:
            item = self._track_list[index]
        except IndexError:
            self.warning("No track found at index %r", index)
        else:
            self.emit('track-removed', index)
            del self._track_list[index]

    def enqueue_album(self, album_id, play_now):
        tracks = self.db.get_tracks_for_album(album_id)
        for track in tracks:
            uri = "file://%s" % track.path
            self._track_list.append((track.id, uri, track.title))
            self.emit('track-added', track, play_now)
            if play_now:
                play_now = False
    
gobject.type_register(Playlist)
