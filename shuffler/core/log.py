
from shuffler.extern.log import log as externlog
from shuffler.extern.log.log import *
import re
import os
import threading
import sys
import gobject

def setDebug(debug_string):
    warn_all = '*:2'
    levels = debug_string.split(',')
    if warn_all not in levels:
        levels.insert(0, warn_all)
        
    externlog.setDebug(','.join(levels))

def init(log_to_file = False):

    externlog.init('SHUF_DEBUG', True)
        
    externlog.setPackageScrubList('shuffler')
    
    if log_to_file == True:
        externlog.outputToFiles('shuffler.log', 'shuffler.log')

    setDebug(os.getenv('SHUF_DEBUG',''))

# Make Loggable a new-style object
class Loggable(externlog.Loggable, object):

    def __init__(self, *args, **kwargs):

        # I don't really like CamelCase
        if hasattr(self, 'log_category'):
            self.logCategory = self.log_category
        else:
            # set log category to class name, lowercased
            self.logCategory = re.sub("([A-Z])",
                                      lambda m: m.groups()[0].lower() \
                                      if not m.start() else \
                                      "_%s" % m.groups()[0].lower(),
                                      self.__class__.__name__)

        super(Loggable, self).__init__()

class GLoggable(Loggable, gobject.GObject):
    pass

gobject.type_register(GLoggable)
