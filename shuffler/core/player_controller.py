
from shuffler.core import log
import parser

class PlayerController(log.Loggable):

    def __init__(self, player):
        super(PlayerController, self).__init__()
        self.player = player

    def handle(self, command=None, origin=None):
        result = ""
        if command:
            parsed = command.split()

            # command is: <method_name> <args>?
            if len(parsed) == 2:
                name, args = parsed
            else:
                name = parsed[0]
                args = ''

            # args are comma separated
            args = args.split(',')
            if args == ['']:
                args = []

            # compile args from strings to python expressions
            compiled_args = []
            for arg in args:
                expr = parser.expr(arg)
                try:
                    compiled_args.append(eval(expr.compile()))
                except NameError, error:
                    self.warning(error)
                    continue
                
            self.info("command=%r args=%r", name, compiled_args)
            if name == "help":
                result = "RTFM"
            else:
                try:
                    result = getattr(self.player, name)(*compiled_args)
                except AttributeError:
                    result = "Unknown command: %r" % name
                else:
                    if result:
                        result = str(result)
        return result
                
