
from math import sqrt
import random
import time

class SmartQueue(object):

    default_config = {
        'update_rating_method': 'conservative', # or 'accelerated'
        'check_rating_method': 'bell', # or 'thresh'
        'repeat_avoid_method': 'count', # or 'age'        
        }

    def __init__(self, db, conf=None):
        self.db = db
        self.config = conf or self.default_config
        self.history = []
        self.current_media_id = None
        self.current_uri = None
        self.current_short_name = None

        self.update_rating_method = self.config['update_rating_method']
        self.min_score = 0
        self.max_score = 100
        self.increase_factor = 10.
        self.decrease_factor = 10.
        # bell curve model
        self.bell = [ 0.0000, 0.0987, 0.1915, 0.2734, 0.3413, 0.3944, 0.4332,
                      0.4599, 0.4772, 0.4878, 0.4938, 0.4970, 0.4987, 0.4994,
                      0.4997, 0.4999, 0.5000 ]

        check_rating_method = self.config['check_rating_method']
        # use bell method by default
        self.check_rating = self._check_bell
        if check_rating_method == 'thresh':
            self.check_rating = self._check_threshold

        # repeat-avoidance 'count' or 'age'
        repeat_avoid_method = self.config['repeat_avoid_method']
        # by default we check by age
        self.check_repeat = self._check_age
        if repeat_avoid_method == 'count':
            self.check_repeat = self._check_count
        self.min_age = 3600 # 1 hour (should be configurable ?)
        self.max_count = 10 #        (idem)
        self.age_bypass_count = 0 # this is set later

    def get_current_item(self):
        """ Return the current uri of the playing queue as a tuple
        (uri, short_name)
        """
        return (self.current_media_id, self.current_uri,
                self.current_short_name)

    def set_current_item(self, media_id, uri, short_name):
        """ Set the current uri of the playing queue as a tuple (uri,
        short_name)
        """
        old = self.get_current_item()
        if old:
            self.history.append(old)
            
        self.current_media_id = media_id
        self.current_uri = uri
        self.current_short_name = short_name
        
        
    def previous_item(self):
        """ Select the previous uri of the queue and return it as a
        tuple (uri, short_name)
        """
        try:
            media_id, uri, short_name = self.history[-1]
        except IndexError:
            media_id = None
            uri = None
            short_name = None
        else:
            self.history = self.history[:-1]
        if media_id:
            self.set_current_item(media_id, uri, short_name)
        return media_id, uri, short_name

    def next_item(self, user_request=False):
        """ Select the next uri of the queue and return it as a tuple
        (uri, short_name)
        """        
        next_location = None
        short_name = None

        media_id, current, short_name = self.get_current_item()

        if current:
            current_media = self.db.get_media_with_id(media_id)
            if user_request:
                self.decrease_rating(current_media)
            else:
                self.increase_rating(current_media)
            self.db.update_media_history(current_media)
            
        good_song = aged_song = False
        medias = self.db.get_medias()
        if medias:
            stats = self.compute_ratings(medias)
            while (not good_song) or (not aged_song):
                # find a candidate
                random_number = random.randint(0,len(medias)-1)
                candidate = medias[random_number]
                # improve it
                good_song = self.check_rating(candidate, stats)
                if good_song:
                    aged_song = self.check_repeat(candidate)
            media = self.db.get_media_with_id(candidate.id)
            next_location, short_name = media.path, media.title
            next_location = "file://%s" % next_location
            self.set_current_item(candidate.id, next_location, short_name)
        return self.get_current_item()

    def compute_ratings(self, medias):
        """ do some stats (used by checkBell & checkThreshold) """
        ratings = [ r.rating for r  in medias ]
        meanCounter = stdDevCounter = 0
        ratingsNumber = float(len(ratings))
        for rating in ratings:
            meanCounter += rating
        mean = meanCounter / ratingsNumber
        for rating in ratings:
            stdDevCounter += (rating - mean)**2
        # standard deviation
        stdDev = sqrt( stdDevCounter / ratingsNumber)
        threshold = stdDev + mean 
        reprieveThreshold = mean - stdDev
        return {'stdDev': stdDev, 'threshold': threshold, 'mean': mean,
                'reprieveThreshold': reprieveThreshold }

    # --- Song rating stuff
    def increase_rating(self,media):
        """ increase the rating of the current queue media """
        currentRating = media.rating
        if self.update_rating_method == 'conservative':
            increase = int((self.max_score - currentRating) / self.increase_factor)
        elif self.update_rating_method == 'accelerated':
            x = currentRating - self.min_score
            y = self.max_score - currentRating
            increase = int( max( [1 , min([x,y])/2.] ) )
        newRating = currentRating + increase
        if newRating > self.max_score:
            newRating = self.max_score
        self.db.update_media(media, rating=newRating)

    def decrease_rating(self, media):
        """ decrease the rating of the current media """
        currentRating = media.rating
        if self.update_rating_method == 'conservative':
            decrease = currentRating / self.decrease_factor
        elif self.update_rating_method == 'accelerated':
            x = currentRating - self.min_score
            y = self.max_score - currentRating
            decrease = int( max( [1 , min([x,y])/2.] ) )
        newRating = currentRating - decrease
        if newRating < self.min_score:
            newRating = self.min_score
        self.db.update_media(media, rating=newRating)

    # --- Repeat avoidance methods
    def _check_age(self, media):
        isAged = False
        currentAge = media.current_age
        if not currentAge:
            iAged = True
        else:
            now = time.time()
            prev = time.mktime(time.strptime(str(currentAge),
                                             '%Y-%m-%d %H:%M:%S'))
            currentAge = now - prev

            if (currentAge >= self.min_age) or (self.age_bypass_count == 0):
                isAged = True
            else:
                self.age_bypass_count -= 1
        return isAged
    
    def _check_count(self, media):
        isAged = False
        countToSkip = media.count_to_skip
        if not countToSkip:
            isAged = True
        else:
            currentCount = countToSkip - 1
            if currentCount <= 0:
                currentCount = 0
                isAged = True
            self.db.update_media(media, count_to_skip=currentCount)
        return isAged

    # --- Rating based choosing methods
    def _check_bell(self, media, stats):
        goodSong = negSigma = False
        floatRandom = random.random()
        if stats['stdDev'] == 0:
            currentSigma = 0
        else:
            preRating = media.rating
            currentSigma = (preRating - stats['mean']) / stats['stdDev']
        if currentSigma < 0:
            currentSigma = - currentSigma
            negSigma = True
        if currentSigma > 4:
            areaSigma = 0.5
        else:
            lowerSigma = int(currentSigma*4)/4.
            upperSigma = (int(currentSigma*4)/4.) + .25
            lowerArea = self.bell[int(currentSigma*4)]
            upperArea = self.bell[int(currentSigma*4)+1]
            ratioSigma = (upperSigma - currentSigma) / (upperSigma - lowerSigma)
            areaSigma = lowerArea * ratioSigma + upperArea * (1 - ratioSigma)
        if negSigma:
            proba = 0.5 - areaSigma / 100.
        else:
            proba = 0.5 + areaSigma / 100.
        if proba > floatRandom:
            goodSong = True
        return goodSong

    def _check_threshold(self, media, stats):
        goodSong = False
        rating = media.rating
        if rating >= stats['threshold']:
            goodSong = True
        elif rating >= stats['reprieveThreshold']:
            rand = random.randint(0,9)
            if rand == 0:
                goodSong = True
        return goodSong

