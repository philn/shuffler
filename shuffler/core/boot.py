
import dbus

def boot():

    from shuffler.core import log
    log.init()


    from dbus.mainloop.glib import DBusGMainLoop
    DBusGMainLoop(set_as_default=True)
    
