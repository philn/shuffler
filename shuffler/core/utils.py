import os

def xdg_dirs():
    xdg_config_dir = os.path.expanduser(os.getenv('XDG_CONFIG_HOME',
                                                  '~/.config'))
    user_dirs_file = os.path.join(xdg_config_dir, 'user-dirs.dirs')
    try:
        lines = [ line.strip() for line in open(user_dirs_file)
                  if not line.startswith('#') ]
    except IOError:
        # file doesn't exist
        dirs = {}
    else:
        dirs = dict([ (line.split('=')[0][4:-4].lower(),
                       os.path.expandvars(eval(line.split('=')[1])))
                       for line in lines ])
    return dirs
