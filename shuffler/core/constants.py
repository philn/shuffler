
BUS_NAME='org.mpris.shuffler'
PLAYER_INTERFACE='org.freedesktop.MediaPlayer'
DB_INTERFACE='org.shuffler.Database'

ROOT_PATH='/'
PLAYER_PATH='/Player'
TRACK_LIST_PATH='/TrackList'
DB_PATH='/Database'
CONFIG_PATH='/Config'
