
from shuffler.core import log
from shuffler.extern import xml_dict, et
from shuffler.core.constants import BUS_NAME, CONFIG_PATH
from shuffler.core.dbus_api.config import DBusConfigAPI

import os
import gobject
import dbus
import dbus.service
import dbus.gobject_service

class Config(dbus.gobject_service.ExportedGObject, log.Loggable,
             DBusConfigAPI):

    SUPPORTS_MULTIPLE_CONNECTIONS = True

    __gsignals__ = {
        'option-changed' : (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE,
                             (gobject.TYPE_STRING,))
        }

    def __init__(self, bus, filename, default_config=None):
        log.Loggable.__init__(self)
        try:
            bus_name = dbus.service.BusName(BUS_NAME, bus)
        except:
            bus_name = None
            self.tube = bus
        else:
            self.tube = None
        dbus.gobject_service.ExportedGObject.__init__(self, bus,
                                                      bus_name=bus_name,
                                                      object_path=CONFIG_PATH)
        DBusConfigAPI.__init__(self)

        self.first_load = False
        self.filename = filename
        self.default_config = default_config or ""
        self._configdict = {}
        self._components = {}

    @property
    def config_dir(self):
        return os.path.dirname(self.filename)

    def __getattr__(self, attrname):
        return self._configdict['settings'][attrname]

    def register(self, Class, config_section):
        instance = Class(dict(config_section))
        name = Class.__name__
        self._components[name] = instance.config
        self.connect('option-changed', instance.config_changed)
        return instance

    def load(self):
        filename = self.filename
        self.info("Loading config from file %r", filename)

        try:
            fd = open(filename)
        except IOError:
            self.first_load = True
            dirname = os.path.dirname(filename)
            if dirname and not os.path.exists(dirname):
                os.makedirs(dirname)
            fd = open(filename, 'w')
            fd.write(self.default_config)
            data = self.default_config
        else:
            data = fd.read()

        fd.close()

        root = et.parse_xml(data).getroot()
        self._configdict = xml_dict.ConvertXmlToDict(root)
        assert 'settings' in self._configdict

    def save(self):
        self.info("Saving config to file %r", self.filename)
        root = xml_dict.ConvertDictToXml(self._configdict)
        tree = et.ET.ElementTree(root)
        et.indent(tree.getroot())
        tree.write(self.filename)
