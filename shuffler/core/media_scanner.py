
from shuffler.core import log
import gobject
import lightmediascanner
import time

class MediaScanner(log.GLoggable):

    __gsignals__ = {
        'scan-finished' : (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE,
                           (gobject.TYPE_STRING,))
        }

    def __init__(self, config=None):
        super(MediaScanner, self).__init__()
        self._config = config or {}

        media_dirs = self._config.get('media_dir')
        if not isinstance(media_dirs, list):
            media_dirs = [media_dirs,]
        self._media_dirs = media_dirs

        self._last_scans = {}

        parsers = self._config['tag_parser']
        charsets = self._config['charset']
        db_path = self._config['db_path']
        slave_timeout = int(self._config['slave_timeout'])
        commit_interval = int(self._config['commit_interval'])

        self._lms = lightmediascanner.LightMediaScanner(db_path, parsers,
                                                        charsets,
                                                        slave_timeout,
                                                        commit_interval)

    def start(self):
        media_dirs = {}
        for opts in self._media_dirs:
            if int(opts['scan_interval']) == -1:
                self.info("Media directory at %r won't be scanned",
                          opts['path'])
            else:
                media_dirs[opts['label']] = opts
        self._event_source_id = gobject.timeout_add(10000,
                                                    self._periodic_scan,
                                                    media_dirs)

    def stop(self):
        gobject.source_remove(self._event_source_id)

    def _scan(self, label, path):
        self.info("Scanning %r at %r", label, path)
        start_time = time.time()
        self._lms.check(path)
        self._lms.process(path)

        end_time = time.time()
        self._last_scans[path] = end_time
        self.info("Scan of %r took %r seconds", label,
                  end_time - start_time)
        self.emit('scan-finished', path)

    def _periodic_scan(self, media_dirs):
        for label, options in media_dirs.iteritems():
            path = options['path']
            scan_interval = int(options['scan_interval'])
            last_scan = self._last_scans.get(path, 0)
            elapsed = time.time() - last_scan
            if elapsed > scan_interval:
                self._scan(label, path)
            else:
                self.debug("Not scanning %r yet. maybe in %r seconds",
                           label, scan_interval-elapsed)
        return True
