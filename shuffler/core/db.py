

from shuffler.core import log
from shuffler.extern import db_row
from shuffler.core.constants import DB_PATH
from shuffler.core.dbus_api.db import DBusDB

import time

try:
    from sqlite3 import dbapi2
except ImportError:
    from pysqlite2 import dbapi2

from dbus.service import Object


"""
CREATE TABLE audio_albums (id INTEGER PRIMARY KEY, artist_id INTEGER, name TEXT);
CREATE TABLE audio_artists (id INTEGER PRIMARY KEY, name TEXT UNIQUE);
CREATE TABLE audio_genres (id INTEGER PRIMARY KEY, name TEXT UNIQUE);
CREATE TABLE audios (id INTEGER PRIMARY KEY, title TEXT, album_id INTEGER, genre_id INTEGER, trackno INTEGER, rating INTEGER, playcnt INTEGER);
CREATE TABLE files (id INTEGER PRIMARY KEY AUTOINCREMENT, path BLOB NOT NULL UNIQUE, mtime INTEGER NOT NULL, dtime INTEGER NOT NULL, size INTEGER NOT NULL);
CREATE TABLE lms_internal (tab TEXT NOT NULL UNIQUE, version INTEGER NOT NULL);
CREATE INDEX audio_albums_artist_idx ON audio_albums (artist_id);
CREATE INDEX audio_albums_name_idx ON audio_albums (name);
CREATE INDEX audio_artists_name_idx ON audio_artists (name);
CREATE INDEX audios_album_idx ON audios (album_id);
CREATE INDEX audios_genre_idx ON audios (genre_id);
CREATE INDEX audios_playcnt_idx ON audios (playcnt);
CREATE INDEX audios_title_idx ON audios (title);
CREATE INDEX audios_trackno_idx ON audios (trackno);
CREATE INDEX files_path_idx ON files (path);
CREATE TRIGGER delete_audio_albums_on_artists_deleted DELETE ON audio_artists FOR EACH ROW BEGIN DELETE FROM audio_albums WHERE artist_id = OLD.id; END;
CREATE TRIGGER delete_audios_on_albums_deleted DELETE ON audio_albums FOR EACH ROW BEGIN DELETE FROM audios WHERE album_id = OLD.id; END;
CREATE TRIGGER delete_audios_on_files_deleted DELETE ON files FOR EACH ROW BEGIN   DELETE FROM audios WHERE id = OLD.id; END;
CREATE TRIGGER delete_audios_on_genres_deleted DELETE ON audio_genres FOR EACH ROW BEGIN DELETE FROM audios WHERE genre_id = OLD.id; END;
CREATE TRIGGER delete_files_on_audios_deleted DELETE ON audios FOR EACH ROW BEGIN DELETE FROM files WHERE id = OLD.id; END;

"""

ADDITIONAL_SCHEMA=["""\
CREATE TABLE media_stats(id INTEGER PRIMARY KEY,
last_played TIMESTAMP,
nb_played INT NOT NULL DEFAULT 0,
current_age TIMESTAMP,
count_to_skip INT NOT NULL DEFAULT 0,
rating FLOAT DEFAULT 50);""",
"""
CREATE TRIGGER delete_media_stats_on_files_deleted DELETE ON files FOR EACH ROW BEGIN   DELETE FROM media_stats WHERE id = OLD.id; END;"""]


class DB(log.Loggable, Object, DBusDB):
    SUPPORTS_MULTIPLE_CONNECTIONS = True
    log_category = 'db'

    def __init__(self, bus, filename):
        log.Loggable.__init__(self)
        Object.__init__(self, bus, DB_PATH)
        DBusDB.__init__(self)
        self.tube = bus
        self.info("Loading db from %r", filename)
        self.filename = filename

    def _check_tables(self):
        for sql in ADDITIONAL_SCHEMA:
            self.sql_execute(sql, quiet=True)

    def open(self):
        self._db = dbapi2.Connection(self.filename)
        self._check_tables()

    def close(self):
        """
        Commit changes to the database and disconnect.
        """
        self.save_changes()
        self._db.close()

    def reopen(self):
        """
        Disconnect and reconnect to the database.
        """
        self.close()
        self.open()

    def save_changes(self):
        """
        Commit changes to the database

        @raise Exception: if the save has failed
        """
        try:
            self._db.commit()
            self.debug('Committed changes to DB')
        except Exception, ex:
            self.debug('Commit failed, %s' % ex)
            raise


    def insert(self, sql_query, *params):
        """ Execute an INSERT SQL query in the db backend
        and return the ID fo the row if AUTOINCREMENT is used

        @param sql_query: the SQL query data to execute
        @type sql_query:  string
        @rtype:           int
        """
        t0 = time.time()

        cursor = self._db.cursor()
        result = -1

        debug_msg = u"%s params=%r" % (sql_query, params)
        debug_msg = u''.join(debug_msg.splitlines())
        self.debug(debug_msg)

        try:
            cursor.execute(sql_query, new_params)
        except Exception, exception:
            self.warning(exception)
        else:
            result = cursor.lastrowid

        cursor.close()
        delta = time.time() - t0
        self.log("SQL insert took %s seconds" % delta)
        
        return result

    def sql_execute(self, sql_query, *params, **kw):
        """ Execute a SQL query in the db backend

        @param sql_query: the SQL query data to execute
        @type sql_query:  string
        @rtype:           L{elisa.extern.db_row.DBRow} list
        """
        t0 = time.time()
        result = self._query(sql_query, *params, **kw)

        delta = time.time() - t0
        self.log("SQL request took %s seconds" % delta)

        return result

    def _query(self, request, *params, **keywords):
        quiet = keywords.get('quiet', False)

        debug_msg = request
        if params:
            debug_msg = u"%s params=%r" % (request, params)
        debug_msg = u''.join(debug_msg.splitlines())
        if debug_msg:
            self.debug('QUERY: %s', debug_msg)

        do_commit = keywords.get('do_commit', False)
        cursor = self._db.cursor()
        result = []
        try:
            cursor.execute(request, params)
        except Exception, exception:
            if not quiet:
                self.warning(exception)
            #FIXME: raise again ?
        else:
            if cursor.description:
                all_rows = cursor.fetchall()
                result = db_row.getdict(all_rows, cursor.description)
        cursor.close()
        if do_commit:
            self.save_changes()
        return result


    def get_artists(self):
        q = "select * from audio_artists order by name"
        results = self.sql_execute(q)
        return results

    def get_albums_for_artist(self, artist_id):
        q = "select audio_albums.id, name from"\
            " audio_albums where artist_id=?"\
            " order by name"
        results = self.sql_execute(q, artist_id)
        return results

    def get_tracks_for_album(self, album_id):
        q = "select audios.id as id, title, trackno, path from audios, files where "\
            " album_id=? and files.id=audios.id"\
            " order by trackno"
        results = self.sql_execute(q, album_id)
        return results

    def get_medias(self):
        request = "select media_stats.* from media_stats, "\
                  "audios where  "\
                  "audios.id=media_stats.id"
        rows = self.sql_execute(request)
        ids = [r.id for r in rows]
        all_audio = "select id from audios"
        for row in self.sql_execute(all_audio):
            if row.id not in ids:
                row.set_extra_attr('rating', 50)
                row.set_extra_attr('last_played', None)
                row.set_extra_attr('nb_played', 0)
                row.set_extra_attr('current_age', None)
                row.set_extra_attr('count_to_skip', 0)
                rows.append(row)
        return rows
    
    def get_media_with_id(self, media_id):
        request = "select files.id as id, path, title from files, audios where "\
                  "files.id=audios.id and audios.id=%s" % media_id
        media = self.sql_execute(request)[0]
        media = self._append_media_stats(media)
        return media

    def _append_media_stats(self, media):
        # look for extra infos
        defaults = {'rating': 50,
                    'last_played': None,
                    'nb_played': 0, 'current_age': None,
                    'count_to_skip': 0}
        request = "select * from media_stats where id=%s" % media.id
        rows = self.sql_execute(request)
        if rows:
            extra = rows[0].as_dict()
        else:
            extra = defaults
        for key, value in extra.iteritems():
            media.set_extra_attr(key, value)
        return media

    def get_full_track_infos(self, media_id):
        media = None
        q = "select audio_albums.id as album_id, "\
            "audio_artists.id as artist_id, "\
            "files.id as id, path, title, trackno, "\
            "audio_albums.name as album, "\
            "audio_artists.name as artist "\
            "from files, audios, audio_albums, audio_artists where "\
            "files.id=audios.id and audios.id=%s "\
            "and audio_albums.id=audios.album_id "\
            "and audio_albums.artist_id=audio_artists.id limit 1" % media_id
        rows = self.sql_execute(q)
        if rows:
            media = rows[0]
            media = self._append_media_stats(media)
        return media
    
    def get_media_with_path(self, path):
        path = "%%%s%%" % path
        request = "select files.id, path, title, album_id, trackno from files, audios where "\
                  "files.id=audios.id and files.path like ?"

        try:
            media = self.sql_execute(request, path)[0]
        except:
            media = None

        # TODO: media_stats

        return media
    

    def update_media(self, media, **kw):
        new_values = []
        for key, value in kw.iteritems():
            new_values.append((key,value))

        if new_values:
            req = "select id from media_stats where id=%s" % media.id
            rows = self.sql_execute(req)
            do_insert = len(rows) == 0
            if do_insert:
                req = "insert into media_stats(id, %s) values (%s, %s)"
                columns = ','.join([str(k) for k,v in new_values])
                col_values = ','.join([str(v) for k,v in new_values])
                req = req % (columns, media.id, col_values)
            else:
                values = ', '.join(["%s=%s" % (str(k),
                                               str(v))
                                    for k,v in new_values])
                req = "update media_stats set %s where id=%s" % (values,
                                                                 media.id)
            self.sql_execute(req)
        self.save_changes()

    def update_media_history(self, media):
        do_insert = False
        curr_stats = "select nb_played from media_stats where id=%s" % media.id
        result = self.sql_execute(curr_stats)
        if result:
            nb_played = result[0].nb_played + 1
        else:
            nb_played = 1
            do_insert = True

        now = time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(time.time()))
        last_played = now
        current_age = now
        count_to_skip = 10
        
        if do_insert:
            query = "insert into media_stats(id, last_played, nb_played, "\
                    "current_age, count_to_skip) values (%s, %r, %s, %r, %s)"
            query = query % (media.id, last_played, nb_played, current_age,
                             count_to_skip)
        else:
            query = "update media_stats set "
            dict_values = {'last_played': repr(last_played),
                           'current_age': repr(current_age),
                           'count_to_skip': count_to_skip,
                           'nb_played': nb_played
                           }
            values = ','.join(["%s=%s" % (k,v)
                               for k,v in dict_values.iteritems()])
            query += "%s where id=%s" % (values, media.id)
        self.sql_execute(query)

    def track_dict(self, media):
        media = media.as_dict()
        media['path'] = unicode(media['path'], 'iso8859-1')
        for key in media.keys()[:]:
            value = media[key]
            if value == None:
                media[key] = ''        
        return media
