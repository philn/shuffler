
from shuffler.core import log
from shuffler.core.constants import BUS_NAME, PLAYER_PATH
from shuffler.core.dbus_api.player import DBusPlayerAPI
from shuffler.core.playlist import Playlist

import time
import gst
import gobject
import dbus
import dbus.service
import dbus.gobject_service

class Player(dbus.gobject_service.ExportedGObject, log.Loggable,
             DBusPlayerAPI):
    SUPPORTS_MULTIPLE_CONNECTIONS = True

    __gsignals__ = {
        'track-ended' : (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE,
                         (gobject.TYPE_FLOAT, gobject.TYPE_PYOBJECT,)),
        'now-playing' : (gobject.SIGNAL_RUN_FIRST, gobject.TYPE_NONE,
                         (gobject.TYPE_PYOBJECT,)),
        'state-changed': (gobject.SIGNAL_RUN_FIRST, gobject.TYPE_NONE,
                          (gobject.TYPE_PYOBJECT,)),
        }

    playing = gobject.property(type=bool, default=False)
    track_start_time = gobject.property(type=float,
                                        default=time.time())

    def __init__(self, bus, db, options=None, queue_options=None,
                 smart_queue_options=None, audiosink_config=None):
        log.Loggable.__init__(self)

        try:
            bus_name = dbus.service.BusName(BUS_NAME, bus)
        except:
            bus_name = None
            self.tube = bus
        else:
            self.tube = None
        dbus.gobject_service.ExportedGObject.__init__(self, bus,
                                                      bus_name=bus_name,
                                                      object_path=PLAYER_PATH)

        DBusPlayerAPI.__init__(self)
        self.options = options or {}
        self._bus = bus
        self._db = db
        self._current_media = {}

        self.queue = Playlist(bus, db, queue_options, smart_queue_options)
        self.queue.connect('track-added', self._track_added)
        self.queue.connect('track-removed', self._track_removed)
        self.queue.connect('current-item-changed',
                           self._current_playlist_item_changed)

        self._speed = 1.
        self._tempo = 1.

    def create_pipeline(self):
        self._pipeline = gst.element_factory_make('playbin')
        pbus = self._pipeline.get_bus()
        pbus.connect('message::eos', self._bus_message_eos_cb)
        pbus.add_signal_watch()
        self.audio_sink = self.options.audiosink.name

        try:
            props = self.options.audiosink.property
            if not isinstance(props, list):
                props = [props,]
        except KeyError:
            props = []

        self.audio_sink_options = props

    def _bus_message_eos_cb(self, bus, message):
        self.info("End of stream reached")

        media = self._get_current_media()
        self.emit('track-ended', self.track_start_time, media)

        self.next_track()

    def _get_current_media(self):
        if 'length' in self._current_media and \
               not self._current_media['length']:
            self._current_media['length'] = self.duration * 1000

        return self._current_media

    def _current_playlist_item_changed(self, obj, infos):
        media_id, uri, short_name = infos
        if media_id:
            db = self.queue.db
            media = db.get_full_track_infos(media_id)
            if media:
                media = db.track_dict(media)
                media['length'] = 0

                del media['path']
                media['URI'] = uri
                self.emit('now-playing', media)
            else:
                media = {}
        else:
            media = {}
        self._current_media = media

    def _track_added(self, obj, media, play_now):
        if play_now:
            location = "file://%s" % media.path
            self.queue.set_current_item(media.id, location, media.title)
            self.play_uri(location)

    def _track_removed(self, obj, index):
        if self.queue.tracks() and self.queue.get_current_item() == self.queue.tracks()[index]:
            self.next_track()

    def start(self):
        self.next_track()

    def stop(self):
        self._pipeline.set_state(gst.STATE_READY)
        self.playing = False
        self.emit('state-changed', self.state)

    @gobject.property
    def state(self):
        return self._pipeline.get_state()[1]

    def get_audio_sink(self):
        return self._audiosink

    def set_audio_sink(self, name):
        # TODO: add support for sink options
        if not name:
            name = "autoaudiosink"

        sink = gst.element_factory_make(name)

        try:
            self._pitch_elt = gst.element_factory_make("pitch", "pitch")
        except gst.ElementNotFoundError, error:
            self.warning("gst pitch element not found.")
            self._pitch_elt = None
            self._pipeline.set_property('audio-sink', sink)
        else:
            audioconvert = gst.element_factory_make("audioconvert")
            capsfilter = gst.element_factory_make("capsfilter")

            bin = gst.Bin()
            bin.add(capsfilter, self._pitch_elt, audioconvert, sink)
            gst.element_link_many(self._pitch_elt, audioconvert, sink)

            capsfilter.link_pads("src", self._pitch_elt, "sink")
            pad = capsfilter.get_pad("sink")
            bin.add_pad(gst.GhostPad("sink", pad))

            self._pipeline.set_property('audio-sink', bin)

        self._audiosink = sink

    audio_sink = gobject.property(getter=get_audio_sink,
                                  setter=set_audio_sink,type=str)

    def get_audio_sink_options(self):
        return self._audio_sink_options

    def set_audio_sink_options(self, options):
        self._audio_sink_options = options
        sink = self._pipeline.get_property('audio-sink')
        for prop in options:
            sink.set_property(prop.name, prop.value)

    audio_sink_options = property(fget=get_audio_sink_options,
                                  fset=set_audio_sink_options)

    def set_volume(self, volume):
        self._pipeline.set_property('volume', volume)

    def get_volume(self):
        return self._pipeline.get_property('volume')

    volume = gobject.property(getter=get_volume,
                              setter=set_volume, type=float)

    def set_mute(self, value):
        self._mute = value
        if value:
            self._saved_volume = self.volume
            self.volume = 0.
        else:
            self.volume = self._saved_volume

    def get_mute(self):
        return self._mute

    muted = gobject.property(getter=get_mute, default=False,
                             setter=set_mute, type=bool)

    def get_position(self):
        try:
            position, format = self._pipeline.query_position(gst.FORMAT_TIME)
            # convert nanosecs to seconds
            position /= float(gst.SECOND)
        except:
            position = 0.
        return position

    def set_position(self, position):
        if position >= 0 and position <= self.duration:
            self.debug("Position set to %s" % position)
            self._seek_to_location(position)
        else:
            self.debug("Invalid position: %r (duration=%r)",
                       position, self.duration)

    position = gobject.property(getter=get_position,
                                setter=set_position, type=float)

    @gobject.property
    def duration(self):
        if self.state not in (gst.STATE_PLAYING, gst.STATE_PAUSED):
            return -1
        try:
            duration, format = self._pipeline.query_duration(gst.FORMAT_TIME)
            # convert nanosecs to seconds
            return duration / float(gst.SECOND)
        except:
            return -1

    def get_current_track(self):
        return self._get_current_media()

    def play_uri(self, uri):
        self._pipeline.set_state(gst.STATE_READY)
        if uri:
            self.info("Now playing %r", uri)
            self.track_start_time = time.time()
            self._pipeline.set_property('uri', uri)
            self._pipeline.set_state(gst.STATE_PLAYING)
            self.playing = True

            self._get_current_media()
        else:
            self.playing = False

    def play(self):
        self.info("Starting to play")
        self._pipeline.set_state(gst.STATE_PLAYING)
        self.playing = True
        self.emit('state-changed', self.state)

    def pause(self):
        self.info("Pausing")
        self._pipeline.set_state(gst.STATE_PAUSED)
        self.emit('state-changed', self.state)

    def toggle_play_pause(self):
        state = self._pipeline.get_state()[1]
        if state == gst.STATE_PLAYING:
            new_state = gst.STATE_PAUSED
        else:
            new_state = gst.STATE_PLAYING
        self._pipeline.set_state(new_state)
        self.emit('state-changed', self.state)

    def next_track(self, user_request=False):
        self._pipeline.set_state(gst.STATE_READY)
        media_id, path, song_title = self.queue.next_item(user_request=user_request)
        if path:
            self.play_uri(path)

    def prev_track(self):
        self._pipeline.set_state(gst.STATE_READY)
        media_id, path, song_title = self.queue.previous_item()
        self.play_uri(path)

    def seek_forward(self, seconds=10):
        self.position += seconds

    def seek_backward(self, seconds=10):
        self.position -= seconds

    def get_speed(self):
        return self._speed

    def set_speed(self, speed):
        if speed == 0.:
            if self._speed > 0:
                speed = -0.1
            else:
                speed = 0.1
        self.info("Setting speed to %r", speed)
        self._speed = speed
        self._seek_to_location(self.position)

    # can't use a gprop here... dunno why gobject.G_MINFLOAT is 0.
    # we need negative float support here, dude.
    speed = property(fget=get_speed, fset=set_speed)

    def set_tempo(self, value):
        if self._pitch_elt:
            self._tempo = value
            self._pitch_elt.set_property('tempo', value)

    def get_tempo(self):
        return self._tempo

    tempo = gobject.property(setter=set_tempo, getter=get_tempo,
                             type=float, default=1.)

    def _seek_to_location(self, location):
        """
        seek to the given location with self.speed
        """

        if location == gst.CLOCK_TIME_NONE or not self._pipeline:
            return

        human_loc = gst.TIME_ARGS(location*gst.SECOND)
        self.info("Seeking to location : %s" % human_loc)

        speed = self.speed

        if speed > 0:
            start = location * gst.SECOND
            end = self.duration * gst.SECOND
        else:
            start = 0
            end = self.position * gst.SECOND

        event = self._pipeline.seek(speed, gst.FORMAT_TIME,
                                gst.SEEK_FLAG_FLUSH | gst.SEEK_FLAG_KEY_UNIT,
                                gst.SEEK_TYPE_SET, start,
                                gst.SEEK_TYPE_SET, end)

gobject.type_register(Player)
