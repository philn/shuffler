# -*- coding: utf-8 -*-

import urllib2, urllib
import re, time, md5, os
import thread, threading
from cPickle import dump, load

from shuffler.core import log

import gobject

UNKNOWN = 'unknown'
SAVED_TRACKS_FILE = os.path.expanduser('~/.shuffler/unsubmitted_tracks.txt')

def quote(some_string):
    try:
        quoted = urllib.quote_plus(some_string)
    except KeyError, exc:
        notfound_char = exc.args[0]
        some_string = some_string.replace(notfound_char, '')
        return quote(some_string)
    return quoted

class ScrobbledTrack:
    def __init__(self, media, start_time):
        encoding = "iso8859-1"
        self.artist = media['artist'].encode(encoding)
        self.album = media['album'].encode(encoding)
        self.name = media['title'].encode(encoding)
        self.length = media['length'] / 1000
        self.track_nb = media['trackno']
        self.start_time = start_time
        
        # MusicBrainz not supported yet
        self.mbid = None
        
    def isSubmitable(self):
        return UNKNOWN not in (self.artist, self.album)

    def __repr__(self):
        return "%r by %r from %r (%r @ %r)" % (self.name, self.artist,
                                               self.album,
                                               self.length,
                                               self.start_time)

    def urlencoded(self, num):
        position = str(num)
        encode = ""
        encode += "a["+position+"]="+ quote(self.artist)
        encode += "&t["+position+"]="+ quote(self.name)
        encode += "&l["+position+"]="+ str(int(self.length))
        encode += "&i["+position+"]="+ str(int(self.start_time))
        encode += "&o["+position+"]=P"
        encode += "&r["+position+"]="
        encode += "&n["+position+"]="+ str(self.track_nb)
        encode += "&b["+position+"]="+quote(self.album)
        
        if self.mbid is not None:
            encode += "&m["+position+"]="+quote(self.mbid)
        else:
            encode += "&m["+position+"]="
        return encode

class NowPlayingTrack:
    def __init__(self, media):
        encoding = "iso8859-1"
        self.artist = media['artist'].encode(encoding)
        self.album = media['album'].encode(encoding)
        self.name = media['title'].encode(encoding)
        self.length = int(media['length'])
        self.track_nb = media['trackno']

        # MusicBrainz not supported yet
        self.mbid = None
        

    def __repr__(self):
        return "%r by %r from %r (%r secs)" % (self.name, self.artist,
                                               self.album,
                                               self.length)

    def urlencoded(self):
        encode = ""
        encode += "a="+ quote(self.artist)
        encode += "&t="+ quote(self.name)
        encode += "&l="+ str(self.length)
        encode += "&n="+ str(self.track_nb)
        encode += "&b="+ quote(self.album)
        
        if self.mbid is not None:
            encode += "&m="+quote(self.mbid)
        else:
            encode += "&m="
        return encode
    

class AudioScrobbler(log.GLoggable):
    log_category = 'audioscrobbler'
    default_config = {'user': '*unset*',
                      'password': '*unset*'}

    def __init__(self, config=None):
        super(AudioScrobbler, self).__init__()
        self.config = config or self.default_config
        self.url = "http://post.audioscrobbler.com/"
        self.user = self.config['user']
        self.password = self.config['password']
        self.client = "tst"
        self.version = "1.0"
        self.failures = 0
        self.wait_timeout = 0
        self.max_timeout = 120 * 60 # 120 minutes
        self.lock = threading.Lock()
        self.loadSavedTracks()
        self._np_timeout = None

    def config_changed(self, *args):
        pass

    def submit_new_track(self, obj, start_time, media):
        #import pdb; pdb.set_trace()
        track = ScrobbledTrack(media, start_time)
        self.submit(track)

    def loadSavedTracks(self):
        self.tracksToSubmit = []
        if os.path.exists(SAVED_TRACKS_FILE):
            f = open(SAVED_TRACKS_FILE, 'r')
            try:
                self.tracksToSubmit = load(f)
            except:
                pass
            f.close()

    def saveTracks(self):
        self.debug('Saving %s tracks' % len(self.tracksToSubmit))
        f = open(SAVED_TRACKS_FILE, 'w')
        dump(self.tracksToSubmit, f)
        f.close()

    def handshake(self, block=False):
        if not block:
            thread.start_new_thread(self._handshake, ())
        else:
            return self._handshake()

    def _handshake(self):
        self.logged = False
        if '*unset*' in (self.user, self.password):
            self.info("Please set username and password")
            return False

        token = md5.md5(md5.md5(self.password).hexdigest() + str(int(time.time()))).hexdigest()
        self.debug("Handshaking (token=%r)...", token)
        url = self.url+"?"+urllib.urlencode({
            "hs":"true",
            "p":"1.2.1",
            "c":self.client,
            "v":self.version,
            "u":self.user,
            "t":str(int(time.time())),
            "a":token,
            })
        try:
            result = urllib2.urlopen(url).readlines()
        except Exception, ex:
            self.debug(ex)
        else:
##             import pdb; pdb.set_trace()
            status = result[0][:-1]
            if status == "OK":
                self.uptodate(result[1:])
                self.logged = True
            elif status == "BANNED":
                self.banned()
            elif status == "BADAUTH":
                self.badauth()
            elif status == "BADTIME":
                self.badtime()
            elif status.startswith("FAILED"):
                self.failures += 1
                self.failed(status, during_handshake=True)
        return self.logged

    def uptodate(self, lines):
        self.md5 = re.sub("\n$","", lines[0])
        self.debug("MD5 = %r", self.md5)
        self.nowplayingurl = re.sub("\n$", "", lines[1])
        self.debug("nowplayingurl = %r", self.nowplayingurl)
        self.submiturl = re.sub("\n$","", lines[2])
        self.debug("submiturl = %r", self.submiturl)
        self.failures = 0
        self.wait_timeout = 0
        return True

    def banned(self):
        self.debug("Yar Bannd")
        return False

    def badtime(self):
        self.debug("Bad time dude. Fix yar clock")
        return False

    def badauth(self):
        self.debug("Bad user")
        return False

    def failed(self, status, during_handshake=False):
        self.debug('Failed : %s' % status)
        if during_handshake:
            if self.failures == 1:
                self.wait_timeout = 60
            else:
                self.wait_timeout *= 2
            if self.wait_timeout > self.max_timeout:
                self.wait_timeout = self.max_timeout
            self.debug("Failure #%r... waiting %r seconds",
                       self.failures, self.wait_timeout)
            time.sleep(self.wait_timeout)
            self.handshake(block=True)
        else:
            if self.failures >= 3:
                self.handshake(block=True)
        return False

##     def interval(self, line):
##         match = re.match("INTERVAL (\d+)", line)
##         if match is not None:
##             secs = int(match.group(1))
##             self.debug("Sleeping a while (%s seconds)" % secs)
##             time.sleep(secs)

    def submit(self, track):
        thread.start_new_thread(self._submit, (track,))

    def _submit(self, track):
        if not self.logged:
            # try to log
            if not self.handshake():
                if track not in self.tracksToSubmit:
                    self.debug("Queued submission of track %s" % track)
                    self.tracksToSubmit.append(track)
                self.debug("%s track(s) currently queued" % len(self.tracksToSubmit))
                self.saveTracks()
                return

        if track not in self.tracksToSubmit:
            self.tracksToSubmit.append(track)

        self.debug("Will try to submit tracks : %s" % str(self.tracksToSubmit))

        try:
            post = "s=%s" % self.md5
            count = 0
            self.debug('post: %r' % post)
            
            for track in self.tracksToSubmit:
                l = int(track.length)
                if not track.isSubmitable():
                    self.debug("Missing informations from track, skipping submit")
                    self.debug(track)
                elif l < 30 or l > (30*60):
                    self.debug("Track is too short or too long, skipping submit")
                    self.debug(track)
                else:
                    self.debug('encoded: %r' % track.urlencoded(count))
                    post += "&"
                    post += track.urlencoded(count)
                    count += 1
        except Exception, ex:
            self.debug('Exception : %s' % ex)

        self.debug('count = %s' % count)
        self.debug(post)
        post = unicode(post)
        if count:
            try:
                result = urllib2.urlopen(self.submiturl,post)
            except Exception,exc:
                self.debug(exc)
                self.logged = False
                #import pdb; pdb.set_trace()
            else:
                results = result.readlines()
                self.debug("submit result : %r" % results)
                if results[0].startswith("OK"):
                    self.tracksToSubmit = []
                    self.saveTracks()
                elif results[0].startswith("FAILED"):
                    self.failures += 1
                    self.failed(results[0])
                elif results[0].startswith("BADSESSION"):
                    self.badauth()
                    self.handshake(block=True)

    def now_playing(self, obj, media):
        if self._np_timeout:
            gobject.source_remove(self._np_timeout)
        self._np_timeout = gobject.timeout_add(1, self._now_playing,
                                               media)
    def _now_playing(self, media):
        track = NowPlayingTrack(media)
        self.debug("Submitting now playing track %r", track)
        
        post = "s=%s" % self.md5
        self.debug('encoded: %r' % track.urlencoded())
        post += "&"
        post += track.urlencoded()
        
        self.debug(post)
        try:
            post = unicode(post)
            result = urllib2.urlopen(self.nowplayingurl, post)
        except Exception,exc:
            self.debug(exc)
        else:
            results = result.readlines()
            self.debug("submit result : %r" % results)
            status = results[0][:-1]
            if status == "OK":
                ok = True
            elif status == "BADSESSION":
                self.badauth()
                self.handshake(block=True)
                self._now_playing(media)
        return False
