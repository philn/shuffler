
import dbus
from dbus.service import method, signal

from shuffler.core.constants import PLAYER_INTERFACE

class DBusConfigAPI:
    """

    This is a mixin, not meant to be used standalone. It is composed
    with L{shuffler.core.config.Config}.

    """

    def __init__(self):
        pass
