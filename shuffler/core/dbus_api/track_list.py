
import dbus
from dbus.service import method, signal

from shuffler.core.constants import PLAYER_INTERFACE

class DBusTrackListAPI(object):
    """

    This is a mixin, not meant to be used standalone. It is composed
    with L{shuffler.core.playlist.Playlist}.
    
    """

    @method(PLAYER_INTERFACE, in_signature='i', out_signature='a{sv}')
    def GetMetadata(self, position):
        if self.smart and position == 0:
            track = self._smart_queue.get_current_item()
            media_id = track[0]
        else:
            try:
                track = self.tracks()[position]
            except IndexError:
                media_id = None
            else:
                media_id = track[0]

        if media_id:
            db = self.db
            media = db.get_full_track_infos(media_id)
            media = db.track_dict(media)
        else:
            media = {}
        track = dbus.Dictionary(media, signature='sv',
                                variant_level=1)
        return track

    @method(PLAYER_INTERFACE, in_signature='', out_signature='i')
    def GetCurrentTrack(self):
        if self.smart:
            index = 0
        elif len(self.tracks()) == 0:
            index = 0
        else:
            index = self._index
        return index

    @method(PLAYER_INTERFACE, in_signature='', out_signature='i')
    def GetLength(self):
        return len(self.tracks())

    @method(PLAYER_INTERFACE, in_signature='', out_signature='aa{sv}')
    def GetTracks(self):
        tracks = []
        if self.smart:
            tracks.append(self.GetMetadata(0))
        else:
            for i in range(len(self.tracks())):
                tracks.append(self.GetMetadata(i))
        return tracks

    @method(PLAYER_INTERFACE, in_signature='sb', out_signature='i')
    def AddTrack(self, uri, play_now):
        self.add_track(uri, play_now)
        return 0

    @method(PLAYER_INTERFACE, in_signature='i', out_signature='')
    def DelTrack(self, position):
        self.del_track(position)

    @method(PLAYER_INTERFACE, in_signature='b', out_signature='')
    def SetLoop(self, value):
        self.loop = value

    @method(PLAYER_INTERFACE, in_signature='', out_signature='b')
    def GetLoop(self):
        return self.loop

    @method(PLAYER_INTERFACE, in_signature='b', out_signature='')
    def SetRandom(self, value):
        self.random = value

    @method(PLAYER_INTERFACE, in_signature='', out_signature='b')
    def GetRandom(self):
        return self.random

    @method(PLAYER_INTERFACE, in_signature='b', out_signature='')
    def SetSmart(self, value):
        self.smart = value

    @method(PLAYER_INTERFACE, in_signature='', out_signature='b')
    def GetSmart(self):
        return self.smart

    @method(PLAYER_INTERFACE, in_signature='ib', out_signature='')
    def EnqueueAlbum(self, album_id, play_now):
        self.enqueue_album(album_id, play_now)
