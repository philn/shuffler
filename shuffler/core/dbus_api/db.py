
import dbus
from dbus.service import method

from shuffler.core.constants import DB_INTERFACE

class DBusDB(object):

    def _row_to_dict(self, row):
        row = row.as_dict()
        if 'path' in row:
            row['path'] = str(row['path'])
        result = dbus.Dictionary(row, signature='sv',
                                 variant_level=1)
        return result

    @method(DB_INTERFACE, in_signature='', out_signature='aa{sv}')
    def ArtistsGet(self):
        artists = self.get_artists()
        return [self._row_to_dict(r) for r in artists]

    @method(DB_INTERFACE, in_signature='i', out_signature='aa{sv}')
    def AlbumsGet(self, artist_id):
        albums = self.get_albums_for_artist(artist_id)
        return [self._row_to_dict(r) for r in albums]
        

    @method(DB_INTERFACE, in_signature='i', out_signature='aa{sv}')
    def TracksGet(self, album_id):
        tracks = self.get_tracks_for_album(album_id)
        return [self._row_to_dict(r) for r in tracks]

    @method(DB_INTERFACE, in_signature='i', out_signature='a{sv}')
    def MediaGet(self, media_id):
        media = self.get_media_with_id(media_id)
        if media:
            media = self._row_to_dict(media)
        else:
            media = {}
        return media
    
