
import gst
import dbus
from dbus.service import method, signal

from shuffler.core.constants import PLAYER_INTERFACE

class DBusPlayerAPI:
    """

    This is a mixin, not meant to be used standalone. It is composed
    with L{shuffler.core.player.Player}.
    
    """

    def __init__(self):
        self.connect('now-playing', self._track_change)
        self.connect('state-changed', self._state_changed)
        
    def _track_change(self, obj, track):
        track = dbus.Dictionary(track, signature='sv',
                                variant_level=1)
        self.TrackChange(track)

    def _state_changed(self, obj, state):
        self.StatusChange(self.GetStatus())

    @signal(PLAYER_INTERFACE, signature='a{sv}')
    def TrackChange(self, track):
        pass

    @signal(PLAYER_INTERFACE, signature='(iiii)')
    def StatusChange(self, status):
        pass

    @signal(PLAYER_INTERFACE, signature='i')
    def CapsChange(self, value):
        pass
    
    @method(PLAYER_INTERFACE, in_signature='', out_signature='s')
    def AudioSinkGet(self):
        return self.audio_sink

    @method(PLAYER_INTERFACE, in_signature='s', out_signature='')
    def AudioSinkSet(self, sink):
        self.audio_sink = sink

    @method(PLAYER_INTERFACE, in_signature='', out_signature='a{sv}')
    def GetMetadata(self):
        track = self.get_current_track()
        return dbus.Dictionary(track,signature='sv',variant_level=1)
    
    @method(PLAYER_INTERFACE, in_signature='i', out_signature='')
    def VolumeSet(self, volume):
        self.volume = volume
        
    @method(PLAYER_INTERFACE, in_signature='', out_signature='i')
    def VolumeGet(self):
        return self.volume

    @method(PLAYER_INTERFACE, in_signature='b', out_signature='')
    def MuteSet(self, value):
        self.muted = value

    @method(PLAYER_INTERFACE, in_signature='', out_signature='b')
    def MuteGet(self):
        return self.muted

    @method(PLAYER_INTERFACE, in_signature='', out_signature='i')
    def PositionGet(self):
        seconds = self.position
        return seconds * 1000

    @method(PLAYER_INTERFACE, in_signature='i', out_signature='')
    def PositionSet(self, msecs):
        seconds = msecs / 1000.
        self.position = seconds

    @method(PLAYER_INTERFACE, in_signature='', out_signature='i')
    def DurationGet(self):
        seconds = self.duration
        msecs = seconds * 1000
        return msecs
        
    @method(PLAYER_INTERFACE, in_signature='', out_signature='')
    def Play(self):
        self.play()
        
    @method(PLAYER_INTERFACE, in_signature='', out_signature='')
    def Pause(self):
        self.pause()

    @method(PLAYER_INTERFACE, in_signature='', out_signature='')
    def Stop(self):
        self.stop()

    @method(PLAYER_INTERFACE, in_signature='', out_signature='')
    def TogglePlayPause(self):
        self.toggle_play_pause()

    @method(PLAYER_INTERFACE, in_signature='', out_signature='')
    def Next(self):
        self.next_track(True)

    @method(PLAYER_INTERFACE, in_signature='', out_signature='')
    def Prev(self):
        self.prev_track()

    @method(PLAYER_INTERFACE, in_signature='i', out_signature='')
    def SeekForward(self, seconds):
        self.seek_forward()

    @method(PLAYER_INTERFACE, in_signature='i', out_signature='')
    def SeekBackward(self, seconds):
        self.seek_backward(seconds)

    @method(PLAYER_INTERFACE, in_signature='', out_signature='d')
    def SpeedGet(self):
        return self.speed

    @method(PLAYER_INTERFACE, in_signature='d', out_signature='')
    def SpeedSet(self, speed):
        self.speed = speed

    @method(PLAYER_INTERFACE, in_signature='', out_signature='d')
    def TempoGet(self):
        return self.tempo

    @method(PLAYER_INTERFACE, in_signature='d', out_signature='')
    def TempoSet(self, tempo):
        self.tempo = tempo

    @method(PLAYER_INTERFACE, in_signature='b', out_signature='')
    def Repeat(self, value):
        pass

    @method(PLAYER_INTERFACE, in_signature='', out_signature='(iiii)')
    def GetStatus(self):
        state = {gst.STATE_PLAYING: 0,
                 gst.STATE_PAUSED: 1,
                 gst.STATE_READY: 2}.get(self.state)
        
        if self.queue.smart or self.queue.random:
            playing_mode = 1
        else:
            playing_mode = 0

        repeat_mode = int(self.queue.loop)

        # hardcoded to "Never give up playing"
        # TODO: update this, maybe.
        never_stop = 1
        
        result = (state, playing_mode, repeat_mode, never_stop)
        return result

    @method(PLAYER_INTERFACE, in_signature='', out_signature='i')
    def GetCaps(self):
        can_go_next = 1 << 0
        can_go_prev = 1 << 1
        can_pause = 1 << 2
        can_play = 1 << 3
        can_seek = 1 << 4
        can_provide_metadata = 1 << 5
        can_has_tracklist = 1 << 6
        mask = can_go_next | can_go_prev | can_pause | \
               can_play | can_seek | can_provide_metadata | \
               can_has_tracklist
        return mask
