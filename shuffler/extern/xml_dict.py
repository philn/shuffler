# Coming from: http://code.activestate.com/recipes/573463/

from shuffler.extern.et import ET as ElementTree

class XmlDictObject(dict):
    def __init__(self, initdict=None):
        if initdict is None:
            initdict = {}
        dict.__init__(self, initdict)
        self._attrs = {}

    def __getattr__(self, item):
        value = self.__getitem__(item)
        return value

    def __setattr__(self, item, value):
        if item == '_attrs':
            object.__setattr__(self, item, value)
        else:
            self.__setitem__(item, value)

    def __str__(self):
        if self.has_key('_text'):
            return self.__getitem__('_text')
        else:
            return ''

    def __repr__(self):
        return repr(dict(self))

    @staticmethod
    def Wrap(x):
        if isinstance(x, dict):
            return XmlDictObject((k, XmlDictObject.Wrap(v)) for (k, v) in x.iteritems())
        elif isinstance(x, list):
            return [XmlDictObject.Wrap(v) for v in x]
        else:
            return x

    @staticmethod
    def _UnWrap(x):
        if isinstance(x, dict):
            return dict((k, XmlDictObject._UnWrap(v)) for (k, v) in x.iteritems())
        elif isinstance(x, list):
            return [XmlDictObject._UnWrap(v) for v in x]
        else:
            return x

    def UnWrap(self):
        return XmlDictObject._UnWrap(self)

def _ConvertDictToXmlRecurse(parent, dictitem):
    assert type(dictitem) is not type([])

    if isinstance(dictitem, dict):
        for (tag, child) in dictitem.iteritems():
            if str(tag) == '_text':
                parent.text = str(child)
##             elif str(tag) == '_attrs':
##                 for key, value in child.iteritems():
##                     parent.set(key, value)
            elif type(child) is type([]):
                for listchild in child:
                    elem = ElementTree.Element(tag)
                    parent.append(elem)
                    _ConvertDictToXmlRecurse(elem, listchild)
            else:
                if not isinstance(dictitem, XmlDictObject):
                    attrs = dictitem
                    dictitem = XmlDictObject()
                    dictitem._attrs = attrs

                if tag in dictitem._attrs:
                    parent.set(tag, child)
                else:
                    elem = ElementTree.Element(tag)
                    parent.append(elem)
                    _ConvertDictToXmlRecurse(elem, child)
    else:
        parent.text = str(dictitem)

def ConvertDictToXml(xmldict):
    roottag = xmldict.keys()[0]
    root = ElementTree.Element(roottag)
    _ConvertDictToXmlRecurse(root, xmldict[roottag])
    return root

def _ConvertXmlToDictRecurse(node, dictclass):
    nodedict = dictclass()
##     if node.items():
##         nodedict.update({'_attrs': dict(node.items())})

    if len(node.items()) > 0:
        # if we have attributes, set them
        attrs = dict(node.items())
        nodedict.update(attrs)
        nodedict._attrs = attrs

    for child in node:
        # recursively add the element's children
        newitem = _ConvertXmlToDictRecurse(child, dictclass)
        if nodedict.has_key(child.tag):
            # found duplicate tag, force a list
            if type(nodedict[child.tag]) is type([]):
                # append to existing list
                nodedict[child.tag].append(newitem)
            else:
                # convert to list
                nodedict[child.tag] = [nodedict[child.tag], newitem]
        else:
            # only one, directly set the dictionary
            nodedict[child.tag] = newitem

    if node.text is None:
        text = ''
    else:
        text = node.text.strip()

    if len(nodedict) > 0:
        # if we have a dictionary add the text as a dictionary value (if there is any)
        if len(text) > 0:
            nodedict['_text'] = text
    else:
        # if we don't have child nodes or attributes, just set the text
        if node.text is not None:
            nodedict = node.text.strip()

    return nodedict

def ConvertXmlToDict(root, dictclass=XmlDictObject):
    return dictclass({root.tag: _ConvertXmlToDictRecurse(root, dictclass)})

